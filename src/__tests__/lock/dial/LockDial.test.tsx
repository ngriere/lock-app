import { screen, render } from '@testing-library/react';
import LockDial from '../../../lock/dial/LockDial';
import userEvent from '@testing-library/user-event';

describe("Lock Dial component should", () => {

    it("render properly", async () => {
        const { asFragment } = render(<LockDial onValueChange={() => { }}  initialValue={0}/>);

        expect(await screen.findAllByRole('button')).toHaveLength(2);
        expect(asFragment).toMatchSnapshot();
    });

    it.each`
        from | to
        ${0} | ${1}
        ${9} | ${0}
`('should notify parent when incrementing from $from to $to', async ({from, to}) => {
        // GIVEN
        const onValueChange = jest.fn();
        render(<LockDial initialValue={from} onValueChange={onValueChange}/> );

        // WHEN
        const incrementButton = await screen.findByRole('button', { name: '+' });
        userEvent.click(incrementButton);

        // THEN
        expect(onValueChange).toHaveBeenCalledTimes(1);
        expect(onValueChange).toHaveBeenCalledWith(to);
    });

    it.each`
        from | to
        ${0} | ${9}
        ${9} | ${8}
`('should notify parent when decrementing from $from to $to', async ({from, to}) => {
        // GIVEN
        const onValueChange = jest.fn();
        render(<LockDial initialValue={from} onValueChange={onValueChange}/> );

        // WHEN
        const incrementButton = await screen.findByRole('button', { name: '-' });
        userEvent.click(incrementButton);

        // THEN
        expect(onValueChange).toHaveBeenCalledTimes(1);
        expect(onValueChange).toHaveBeenCalledWith(to);
    });

});