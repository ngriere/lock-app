import LockDial from './dial/LockDial';
import { useState } from 'react';

const Lock: React.FC = () => {

    const initialCode: [number, number, number, number] = [0, 0, 0, 0];

    const [code, setCode] = useState(initialCode);

    const codeChange = (changedDigit: number, key: number) => {
        setCode((code) => Object.assign([], code, { [key]: changedDigit }));
    }

    return <div>
        <h2>Code : {code.join('')}</h2>
        <div style={{ display: 'flex' }}>
            {code.map((digit, index) => <LockDial initialValue={digit} key={index}
                onValueChange={(changedDigit) => codeChange(changedDigit, index)} />)}
        </div>
    </div>;
};

export default Lock;