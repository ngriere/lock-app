import { useEffect, useState } from "react";

type Props = {
    initialValue: number
    onValueChange: (value: number) => void;
}

const LockDial: React.FC<Props> = ({ initialValue, onValueChange }) => {
    let [currentValue, setCurrentValue] = useState(initialValue);

    const increment = () => setCurrentValue((value) => {
        if (value === 9) {
            return 0;
        }
        return value + 1;
    });

    const decrement = () => setCurrentValue((value) => {
        if (value === 0) {
            return 9;
        }
        return value - 1;
    });

    useEffect(() => {
        if (currentValue !== initialValue) {
            onValueChange(currentValue);
        }
    }, [initialValue, currentValue, onValueChange]);

    return <div style={{ flex: 'auto' }}>
        <button onClick={increment}>+</button>
        <p>{currentValue}</p>
        <button onClick={decrement}>-</button>
    </div>
};

export default LockDial;