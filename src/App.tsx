import React from 'react';
import './App.css';
import Lock from './lock/Lock';

function App() {
  return (
    <div className="App">
      <h1>Lock App</h1>
      <Lock />
    </div>
  );
}

export default App;
